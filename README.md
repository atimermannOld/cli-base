


## Descrição

Um framework para criação de ferramentas de linha de comando, seu foco são aplicações grandes com muitos scripts.
Facilita organização e diminui trabalho com configuração de argumentos entre outros


## Exemplo de Uso

    cli.scriptDir = '../scripts';

    cli.help = function () {


        var help =
            "----------------------------------------\n" +
            "Configurando Ambiente de Desenvolvimento\n" +
            "----------------------------------------\n" +

                // TODO: Explicar como linkar o projeto direto no BIN do sistema pra ficar disponível instantaneamente
                // TODO: Explicar como linkar um projeto ao sindri no BITBICUCKET

            "----------------------------------------\n" +
            "Ajuda de comandos\n" +
            "----------------------------------------\n" +
            "sindri help ".red + "<command-name>".red.bold;

        util.print(help + "\n");


    };

    cli.load(__dirname);

* O Método **help** deve ser reescrito.
* **scriptDir** é o caminho onde os script ficarão
* O script deve ter o nome que será chamado na linha de comando exemplo:


    bdtool create-file

Devendo existir **create-file.js** na pasta **scriptDir**


## Criação de Scripts
Os scripts devem herdar o objeto cliBase e implementar alguns métodos:

### help
Imprime ajuda ao digitar help <comando>

### getShortInfo
Imprime descrição curta ao lista comandos

### main
Método de entrada, será chamado ao executar o comando relacionado ao seu script


##Exemplo:
    var CliBase = require('cli-base').cliBase;

    module.exports = Object.create(CliBase, {

        main: {
            value: function (argv) {
                "use strict";

        
            }
        }

    });


