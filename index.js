/**
 * Created by andre on 23/11/14.
 */

// Módulo que inicia sistema
exports.cli = require("./lib/cli");

// Módulo que todos os scripts devem herdar
exports.cliBase = require("./lib/cli_base");