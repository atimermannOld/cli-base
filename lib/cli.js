/**
 * Created by andre on 23/11/14.
 */
'use strict';

var CliBase = require('./cli_base'),
    fs = require('fs'),
    path = require('path'),
    _ = require('lodash'),
    util = require('util');

require('colors');

var Cli = {

    _scripts: {},

    scriptDir: "scripts",

    /**
     * Caminho raiz, normalmente __dirname
     */
    rootPath: null,

    /**
     * Inicializa sistema que gerencia scripts Cli
     *
     * Deve ser passado no primeiro argumento o nome do script. Ex: dbtool create-project
     *
     * O primeiro argumento será o nome do script sem extensão, localizado no diretório /scripts
     *
     * Neste método é carregado todos os script armazenado nesta pasta
     *
     *
     * @param rootPath  Caminho Raiz do projeto, normalmente __dirname
     */
    load: function (rootPath) {


        var self = this;

        if (typeof rootPath !== "undefined") {
            self.rootPath = rootPath;
        }

        if (typeof self.rootPath !== 'string') {
            throw new Error('rootPath deve ser definido');
        }

        //////////////////////////////////////////////////////////
        // Carrega scripts do sistema
        //////////////////////////////////////////////////////////

        if (!Array.isArray(self.scriptDir)){
            self.scriptDir = [self.scriptDir];
        }

        self.scriptDir.forEach(function (scriptDir) {

            var files = fs.readdirSync(scriptDir);

            files.forEach(function (file) {


                var s = require(path.join(scriptDir, file));

                if (typeof s === "object") {

                    if (Object.getPrototypeOf(s) === CliBase) {

                        // Define nome do Script
                        s.scriptName = file.slice(0, -3);

                        self._scripts[s.scriptName] = s;

                    } else {

                        console.log("WARNING:".red.bold + " Script '%s' inválido! Deve herdar ".red + "cliBase".red.bold, file)

                    }
                }

            });


        });

        self.extractArguments();

    },

    /**
     * Extrai Argumentos. Lê argumentos passado pelo usuário, e automaticamente carrega o script correto, caso não
     * encontra gera aviso ou se não for passado nenhum argumento impreme mensagem de ajuda
     *
     */
    extractArguments: function () {


        var self = this;

        var usage = "sindri".red + " <comando>".red.bold + "\n\nLista de comandos:\n";

        Object.keys(self._scripts).forEach(function (key) {

            var s = self._scripts[key];
            usage += "\t" + s.scriptName.red.bold + ": " + s.getShortInfo().cyan + "\n";

        });


        //Ajuda
        usage += "\t" + "help:".red.bold + " Ajuda do comando, ex: sindri help <nome-do-comando>\n".cyan;


        //////////////////////////////////////////////////////////////////////////////////////
        // Extrai argumentos usando yargs
        /////////////////////////////////////////////////////////////////////////////////////
        var argv = require('yargs')
            .usage(usage)
            .demand(1)
            .argv;

        var option = argv._[0];

        ///////////////////////////////////////////////////////////////////////////////////////
        // Apenas um comando está disponível, o help, para todos os outros será feito uma tentativa de carregar
        // externamente
        ///////////////////////////////////////////////////////////////////////////////////////

        var command = argv._[0];

        if (command === "help") {
            var scriptName = argv._[1];

            if (scriptName) {

                if (this._scripts[scriptName]) {

                    util.puts("Ajuda: " + scriptName.red.bold + "\n");

                    util.puts(this._scripts[scriptName].help());


                    var args = this._scripts[scriptName].arguments();

                    if (args) {

                        util.puts("\nArgumentos:\n".bold);

                        _.forEach(args, function (description, argument) {


                            console.log("\t%s", argument);
                            console.log("\t\t%s\n", description);

                        });
                    }


                } else {

                    util.puts("Comando ".red + scriptName.red.bold + " não encontrado".red);
                }


            } else {

                self.help();

            }
        }

        ////////////////////////////////////////////
        // Executa comando caso encontre no sistema
        ////////////////////////////////////////////
        else {
            if (this._scripts[command]) {

                this._scripts[command].main(argv);

            } else {

                console.log("Comando ".red + command.red.bold + " não encontrado".red);

            }
        }


    },

    /**
     * Imprime Ajuda
     */
    help: function () {

        console.log('[ Ajuda não foi escrita ]'.red);

    }

};

module.exports = Cli;