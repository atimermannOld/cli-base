/**
 * Created by andre on 23/11/14.
 */


var prompt = require('prompt');
var util = require('util');
prompt.message = "";
prompt.delimiter = " ";
prompt.colors = false;
prompt.start();


var promptBool = {
    name: 'promptBool',
    message: '#',
    validator: /^[sSnN]$/,
    warning: 'Responda s (sim) ou n (não)',
    default: 'n'
};

module.exports = {


    /**
     * Imprime Ajuda
     * @returns {string}
     */
    help: function () {
        return "[ Ajuda não foi escrita ]"
    },

    arguments: function () {

        return null

    },

    /**
     * Retorna Informações curta do script
     */
    getShortInfo: function () {
        "use strict";

        return "[ Ajuda curta não foi escrita ]";

    }
    ,

    main: function () {
        "use strict";

        throw "Método 'start' Deve ser implementado";

    },

    /**
     * Este método auxilia passar muitos argumentos para uma função, é possível, rapidamente, especificar quais
     * atributos serão copiados, quais atributos serão copiados alterando o nome e caso existam alias de atributos nos
     * argumentos, copiar o atributo que existir para outro
     *
     * Primeiro se cria um objeto options(que será o destino), passando na contrução o objeto a ser copiado(Origem)
     *
     * Ex:
     *      var destino = self.configureArgs(origem);
     *
     * Para definir quais parâmetros serão copiados usar:
     *
     * destino.c('nome-do-atributo');
     * destino.c('novo-nome-do-atributo', 'antigo-atributo');
     * destino.c('novo-nome-do-atributo', ['atributo', 'alias-do-atributo']);
     *
     * NOTA: Como foi baseado no yargs, não fiz a opção de inverter, pois pode-se usar o prefixo no-
     *
     * @param from
     * @returns {{}}
     */
    configureArgs: function (from) {

        // PARA NEGAR, usar prefixo no- (vem do yargs)
        // TODO: Colocar no aUtil

        var options = {};

        // Copia atributo de from p/ options, seguindo algumas regras
        Object.defineProperty(options, 'c', {
            value: function () {


                var self = this;
                var args = arguments;

                // Se existe 2 argumento e ele é array
                if (args[1] !== undefined && Array.isArray(args[1])) {


                    args[1].forEach(function (atrib) {

                        if (from[atrib] !== undefined) {
                            self[args[0]] = from[atrib];
                        }

                    });

                }
                // Se Existe 2 segumento mas não é array
                else if (args[1] !== undefined) {

                    if (from[args[1]] !== undefined) {
                        self[args[0]] = from[args[1]];
                    }


                }
                // Se não existe segundo argumento
                else {

                    if (from[args[0]] !== undefined) {
                        self[args[0]] = from[args[0]];
                    }
                }

                return this;
            }

        });

        return options;

    },

    /**
     * Exemplo de Uso:
     *  this
     *        .readBool("Diretório não está vazio, continuar?")
     *        .yes(function () {
     *
     *            self.createPackageReadFile();
     *
     *        });
     *
     * @param question
     * @returns {{yes: yes, no: no}}
     */
    readBool: function (question) {
        'use strict';

        var yes,
            no;


        util.puts(question)
        prompt.get(promptBool, function (err, data) {

            if (err) {
                throw err;
            }

            /** @namespace data.promptBool */
            if (data.promptBool.toLowerCase() === 's' || data.promptBool.toLowerCase() === 'sim' && yes) {

                yes();

            } else if (no) {
                no();

            }
        });

        return {
            yes: function (fn) {

                yes = fn;

            },
            no: function (fn) {
                no = fn;

            }
        };

    }

}
;
